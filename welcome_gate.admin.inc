<?php
// $Id$

/**
 * @file
 * Admin page callback for the Welcome gate module.
 */

/**
 * Builds and returns the Welcome gate settings form.
 */
function welcome_gate_admin_settings() {
  $form['welcome_gate_location'] = array(
    '#type' => 'textfield',
    '#title' => t('Welcome gate path'),
    '#default_value' => variable_get('welcome_gate_location', 'welcome'),
    '#description' => t('Enter a local path, without a leading /'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}